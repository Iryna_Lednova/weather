import React from 'react';
import { observer } from 'mobx-react-lite';
import { format } from 'date-fns/esm';
import useStore from '../hooks/useStore';

const Header = observer(() => {
  const { weatherDataStore } = useStore();
  const { weatherType, selectedDay } = weatherDataStore;

  const isReady = weatherType && selectedDay;

  return (
    <div className="head">
      {isReady && (
        <>
          <div className={`${weatherType} icon`} />
          <div className="current-date">
            <p>{format(new Date(selectedDay), 'EEEE')}</p>
            <span>{format(new Date(selectedDay), 'dd LLLL')}</span>
          </div>
        </>
      )}
    </div>
  );
});
export default Header;

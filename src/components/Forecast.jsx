import React from 'react';
import { observer } from 'mobx-react-lite';
import { action } from 'mobx';
import { format } from 'date-fns/esm';
import useStore from '../hooks/useStore';
import useSetDefaultDay from '../hooks/useSetDefaultDay';
import useApplyFilter from '../hooks/useApplyFilter';
import useGetWeatherData from '../hooks/useGetWeatherData';

const Forecast = observer(() => {
  const { data, isFetched } = useGetWeatherData();
  const days = useApplyFilter(data);
  useSetDefaultDay(days);

  const { weatherDataStore } = useStore();
  const { selectedDay } = weatherDataStore;

  const handleSetDay = action((dateInfo) => weatherDataStore.setDayInfo(dateInfo));

  const weatherOfTheDay =
    isFetched &&
    (days.length ? (
      days.map((dateInfo) => (
        <div
          role="button"
          tabIndex={0}
          className={`day ${dateInfo.type} ${selectedDay === dateInfo.day && 'selected'}`}
          key={dateInfo.id}
          onClick={() => handleSetDay(dateInfo)}
          onKeyDown={() => handleSetDay(dateInfo)}
        >
          <p>{format(new Date(dateInfo.day), 'EEEE')}</p>
          <span>{dateInfo.temperature}</span>
        </div>
      ))
    ) : (
      <p className="message">No days available!</p>
    ));

  return <div className="forecast">{isFetched ? weatherOfTheDay : null}</div>;
});
export default Forecast;

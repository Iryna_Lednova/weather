import React from 'react';
import { observer } from 'mobx-react-lite';
import useStore from '../hooks/useStore';

const CurrentWeather = observer(() => {
  const { weatherDataStore } = useStore();
  const { temperature, rainProbability, humidity } = weatherDataStore;
  const isReady = !!(temperature && rainProbability && humidity);

  return (
    <div className="current-weather">
      {isReady && (
        <>
          <p className="temperature">{temperature}</p>
          <p className="meta">
            <span className="rainy">{rainProbability}%</span>
            <span className="humidity">{humidity}%</span>
          </p>
        </>
      )}
    </div>
  );
});
export default CurrentWeather;

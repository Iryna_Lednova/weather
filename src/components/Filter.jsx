import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import useStore from '../hooks/useStore';

const Filter = observer(() => {
  const [isFiltered, setIsFiltered] = useState(false);
  const [weatherType, setWeatherType] = useState('');
  const { sortStore } = useStore();
  const {
    register,
    handleSubmit,
    setValue,
    reset,
    formState: { isDirty },
  } = useForm({
    mode: 'onTouched',
  });

  const handleFilter = handleSubmit((data) => {
    sortStore.applyFilter(data);
    if (isFiltered) {
      sortStore.resetFilter();
      reset();
      setValue('type', '');
      setValue('minTemperature', '');
      setValue('maxTemperature', '');
      setWeatherType('');
    }
    setIsFiltered(!isFiltered);
  });

  const handleSetWeatherType = (value) => {
    if (!isFiltered) {
      setValue('type', value, { shouldTouch: true });
      setWeatherType(value);
    }
  };

  const getWeatherTypeClassName = (type) =>
    classNames({
      checkbox: true,
      selected: weatherType === type,
      disabled: isFiltered,
    });

  return (
    <form className="filter" onSubmit={handleFilter}>
      {['Sunny', 'Cloudy'].map((value) => (
        <span
          {...register('type')}
          key={value}
          role="button"
          tabIndex={0}
          onClick={() => handleSetWeatherType(value.toLocaleLowerCase())}
          onKeyDown={() => handleSetWeatherType(value.toLocaleLowerCase())}
          className={getWeatherTypeClassName(value.toLocaleLowerCase())}
        >
          {value}
        </span>
      ))}
      <p className="custom-input">
        <label htmlFor="min-temperature">Min temperature</label>
        <input
          {...register('minTemperature')}
          type="number"
          name="minTemperature"
          id="minTemperature"
          disabled={isFiltered}
        />
      </p>
      <p className="custom-input">
        <label htmlFor="max-temperature">Max temperature</label>
        <input
          {...register('maxTemperature')}
          type="number"
          name="maxTemperature"
          id="maxTemperature"
          disabled={isFiltered}
        />
      </p>
      <button type="submit" disabled={!isDirty}>
        {isFiltered ? 'Reset' : 'Filter'}
      </button>
    </form>
  );
});
export default Filter;

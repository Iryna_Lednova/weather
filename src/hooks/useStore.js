import { useContext } from 'react';
import { StoreContext } from '../lib/providers/ContextProvider';

const useStore = () => {
  const store = useContext(StoreContext);

  return store;
};
export default useStore;

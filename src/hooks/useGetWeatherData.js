import { useQuery } from 'react-query';
import api from '../api/api';

function useGetWeatherData() {
  const query = useQuery(`weather`, () => api.getWeather());
  const { data, isFetched, isLoading } = query;

  return {
    data,
    isFetched,
    isLoading,
  };
}
export default useGetWeatherData;

import { format } from 'date-fns';
import { action } from 'mobx';
import { useEffect } from 'react';
import useStore from './useStore';

const useSetDefaultDay = (days) => {
  const { weatherDataStore } = useStore();
  useEffect(
    action(() => {
      if (!weatherDataStore.selectedDay) {
        const currentDay = new Date();
        let selectedDay = days?.find(
          (dataInfo) => format(currentDay, 'yyyy-MM-dd') === format(new Date(dataInfo.day), 'yyyy-MM-dd'),
        );
        if (!selectedDay && days.length) {
          [selectedDay] = days;
        }
        weatherDataStore.setDayInfo(selectedDay);
      }
    }, []),
  );
};
export default useSetDefaultDay;

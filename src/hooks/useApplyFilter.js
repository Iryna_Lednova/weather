import { useEffect } from 'react';
import useStore from './useStore';

const useApplyFilter = (days) => {
  const { sortStore, weatherDataStore } = useStore();
  const data = sortStore.filteredDays(days);
  useEffect(() => {
    weatherDataStore.resetDayInfo();
  }, [data]);
  return data;
};
export default useApplyFilter;

// Core
import axios from 'axios';
import WEATHER_API_URL from './config';

const api = Object.freeze({
  async getWeather(limit) {
    let url = WEATHER_API_URL;
    if (limit > 0) {
      url = `${url}?limit=${limit}`;
    }
    const {
      data: { data: response },
    } = await axios.get(url);

    return response;
  },
});
export default api;

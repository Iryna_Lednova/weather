const WEATHER_API_URL = `${process.env.REACT_APP_ROOT_API_URL}/rtx/api/forecast`;
export default WEATHER_API_URL;

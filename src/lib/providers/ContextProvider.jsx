// Core
import React, { createContext } from 'react';
import RootStore from '../mobx/rootStore';

// Store

export const rootStore = new RootStore();
export const StoreContext = createContext(rootStore);

const StoreContextProvider = ({ children }) => {
  return <StoreContext.Provider value={rootStore}>{children}</StoreContext.Provider>;
};
export default StoreContextProvider;

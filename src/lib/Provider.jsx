// Core
import React, { createContext } from 'react';

// Store
import store from './mobx/weatherStore';

export const Context = createContext(store);

const Provider = ({ children }) => {
  return <Context.Provider value={store}>{children}</Context.Provider>;
};
export default Provider;

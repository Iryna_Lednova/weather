// Core
import { makeAutoObservable } from 'mobx';
import { computedFn } from 'mobx-utils';

class SortWeatherStore {
  type = '';
  minTemperature = null;
  maxTemperature = null;
  isFiltered = false;
  limit = 7;
  /**
   * Function (days: Array<day>) => Array<day>
   *
   * Returns: days filtered by filters set in SortWeatherStore object
   */
  filteredDays = {};

  constructor() {
    this.filteredDays = computedFn((days) => {
      const filteredDays = days?.filter((day) => {
        const isCorrectType = this.type ? this.type === day.type : true;
        const isCorrectMinTemperature = this.minTemperature
          ? Number(this.minTemperature) <= Number(day.temperature)
          : true;
        const isCorrectMaxTemperature = this.maxTemperature
          ? Number(this.maxTemperature) >= Number(day.temperature)
          : true;

        return isCorrectType && isCorrectMinTemperature && isCorrectMaxTemperature;
      });

      return filteredDays ? filteredDays.slice(0, this.limit) : [];
    });

    makeAutoObservable(this);
  }

  // setType(type) {
  //   this.type = type;
  // }

  // setMinTemperature(temp) {
  //   this.minTemperature = temp;
  // }

  // setMaxTemperature(temp) {
  //   this.maxTemperature = temp;
  // }

  applyFilter(filter) {
    if (filter.type) {
      this.type = filter.type;
    }

    if (filter.minTemperature) {
      this.minTemperature = filter.minTemperature;
    }

    if (filter.maxTemperature) {
      this.maxTemperature = filter.maxTemperature;
    }

    this.isFiltered = true;
  }

  get isFormBlocked() {
    return this.type === '' && this.minTemperature === '' && this.maxTemperature === '';
  }

  resetFilter() {
    this.maxTemperature = '';
    this.minTemperature = '';
    this.type = '';
    this.isFiltered = false;
  }
}

export default SortWeatherStore;

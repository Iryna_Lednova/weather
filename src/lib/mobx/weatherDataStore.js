import { makeAutoObservable } from 'mobx';

class WeatherDataStore {
  _selectedDay = null;
  _weatherType = '';
  _temperature = null;
  _rainProbability = null;
  _humidity = null;

  constructor(rootStore) {
    makeAutoObservable(
      this,
      { rootStore: false },
      {
        autoBind: true,
      },
    );
    this.rootStore = rootStore;
  }

  set selectedDay(day) {
    this._selectedDay = day;
  }

  get selectedDay() {
    return this._selectedDay;
  }

  set weatherType(type) {
    this._weatherType = type;
  }

  get weatherType() {
    return this._weatherType;
  }

  set temperature(t) {
    this._temperature = t;
  }

  get temperature() {
    return this._temperature;
  }

  set rainProbability(probability) {
    this._rainProbability = probability;
  }

  get rainProbability() {
    return this._rainProbability;
  }

  set humidity(h) {
    this._humidity = h;
  }

  get humidity() {
    return this._humidity;
  }

  setDayInfo(dateInfo) {
    if (dateInfo && this.dayId !== dateInfo?.id) {
      this.selectedDay = dateInfo?.day;
      this.weatherType = dateInfo?.type;
      this.temperature = dateInfo?.temperature;
      this.rainProbability = dateInfo?.rain_probability;
      this.humidity = dateInfo?.humidity;
      this.dayId = dateInfo?.id;
    }
  }

  resetDayInfo() {
    this.selectedDay = null;
    this.weatherType = '';
    this.temperature = null;
    this.rainProbability = null;
    this.humidity = null;
    this.dayId = null;
  }
}
export default WeatherDataStore;

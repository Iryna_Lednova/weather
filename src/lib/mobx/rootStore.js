import { makeAutoObservable } from 'mobx';
import SortWeatherStore from './sortWeatherStore';
import WeatherDataStore from './weatherDataStore';

class RootStore {
  weatherDataStore = null;
  sortStore = null;

  constructor() {
    makeAutoObservable(this);
    this.weatherDataStore = new WeatherDataStore(this);
    this.sortStore = new SortWeatherStore(this);
  }
}
export default RootStore;

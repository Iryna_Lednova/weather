// Core
import React from 'react';
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { configure } from 'mobx';

// Components
import App from './app';

// Instruments
import './theme/index.scss';
import queryClient from './lib/react-query/query-client';
import StoreContextProvider from './lib/providers/ContextProvider';

configure({
  enforceActions: 'always',
  computedRequiresReaction: true,
  observableRequiresReaction: true,
  reactionRequiresObservable: true,
});

render(
  <StoreContextProvider>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </StoreContextProvider>,
  document.getElementById('root'),
);

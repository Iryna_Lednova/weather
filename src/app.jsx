import React from 'react';
import { observer } from 'mobx-react-lite';
import CurrentWeather from './components/CurrentWeather';
import Forecast from './components/Forecast';
import Header from './components/Header';
import Filter from './components/Filter';

const App = observer(() => {
  return (
    <main>
      <Filter />
      <Header />
      <CurrentWeather />
      <Forecast />
    </main>
  );
});
export default App;
